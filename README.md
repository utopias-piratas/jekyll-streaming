# Streaming

Genera un sitio que muestra un stream de video en distintos formatos a
partir de una fuente Icecast.

El sitio se encarga de recuperar la transmisión cuando se corta.

## Uso

Modificar el archivo `_config.yml`:

```yaml
title: MIAU Dev
lang: es
markdown: kramdown
streaming:
  url: https://trans.kefir.red
  stream: miaudev
  formats:
    - webm
    - mp4
```

El parámetro `url` es la dirección base del servidor Icecast.  `stream`
es el nombre del video, el `canal` en terminología de Icecast.

Los `formats` son los formatos de video disponibles, al menos tiene que
haber uno.

Colocar en el archivo `assets/cover.jpg` la portada de los videos y
en `assets/favicon.gif` el icono del sitio.
