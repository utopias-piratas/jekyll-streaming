---
---

const videoEl = document.querySelector('video');

let waiter = null;

function streaming() {
  // Cambiar el src para que incluya un timestamp, esto engaña a Firefox
  // a recargar el video cuando se corta en lugar de pensar que terminó
  // la descarga y empezar a reproducir desde el principio.
  for (const el of videoEl.querySelectorAll('source')) {
    el.src = `${el.src.split('?')[0]}?${Date.now().toString()}`;
  }

  // Empezar la reproducción
  videoEl.load();
  videoEl.play();
}

// Cuando estamos reproduciendo dejamos de esperar y limpiamos todas
// las recargas después de un error
videoEl.addEventListener('playing', e => waiter && clearInterval(waiter));

// Volver a reproducir cuando se corte
videoEl.addEventListener('ended', event => {
  console.log('Reiniciando transmisión');
  streaming();
});

// Si falla la carga, por ejemplo mientras vuelve la transmisión,
// reintentar regularmente.  Si ya estamos intentando, no hacer nada.
for (const el of videoEl.querySelectorAll('source')) {
  el.addEventListener('error', event => {
    // Si no hay una tarea esperando, configurarla
    if (!waiter) {
      console.log('Falló la carga de la fuente, reiniciando transmisión.');
      // Reintentar cada 2 segundos.
      waiter = setInterval(streaming, 2000);
    }
  });
}
