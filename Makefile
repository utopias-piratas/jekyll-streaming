all: build compress

build:
	bundle exec jekyll build

compress:
	find _site -type f | xargs gzip -fk
